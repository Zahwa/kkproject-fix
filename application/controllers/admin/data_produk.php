<?php

class Data_produk extends CI_Controller{
    public function index()
    {
        $data['produk'] = $this->model_produk->tampil_data()->result();
        $this->load->view('templates_admin/header');
        $this->load->view('templates_admin/sidebar');
        $this->load->view('admin/data_produk',$data);
        $this->load->view('templates_admin/footer');
    }
    public function tambah_aksi()
    {
        $nama_produk      = $this->input->post('nama_produk');
        $harga       = $this->input->post('harga');
        $gambar       = $_FILES['gambar']['name'];
        if ($gambar=''){}else{
            $config ['upload_path'] = './uploads';
            $config ['allowed_types'] = 'jpg|jpeg|png|gif|';

            $this->load->library('upload', $config);
            if(!$this->upload->do_upload('gambar')){
                echo "Gambar gagal diupload";
            }else{
                $gambar=$this->upload->data('file_name');
            }
        }
        $data = array(
            'nama_produk'      => $nama_produk,
            'harga'         => $harga,
            'gambar'        => $gambar
        );
        $this->model_produk->tambah_produk($data, 'tb_produk');
        redirect('admin/data_produk/index');
    }

    public function edit($id)
    {
        $where = array('id_produk' =>$id);
        $data['produk'] = $this->model_produk->edit_produk($where,'tb_produk')->result();
        $this->load->view('templates_admin/header');
        $this->load->view('templates_admin/sidebar');
        $this->load->view('admin/edit_produk',$data);
        $this->load->view('templates_admin/footer');
    }

    public function update()
    {
        $id_produk                 = $this->input->post('id_produk');
        $nama_produk          = $this->input->post('nama_produk');
        $harga              = $this->input->post('harga');

        $data = array(
                'nama_produk'      => $nama_produk,
                'harga'         => $harga,
        );
        $where = array(
            'id_produk'    => $id_produk
        );

        $this->model_produk->update_data($where,$data,'tb_produk');
        redirect('admin/data_produk/index');
    }

    public function hapus($id)
    {
        $where = array('id_produk' => $id);
        $this->model_produk->hapus_data($where, 'tb_produk');
        redirect('admin/data_produk/index');
    }
}