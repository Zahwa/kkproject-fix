<?php

class Dashboard extends CI_Controller{

    public function index()
    {
        $data['barang'] = $this->model_produk->tampil_data()->result();
        $this->load->model('model_produk');
        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('dashboard',$data);
        $this->load->view('templates/footer');
    }
}